package com.vladyslav.obochuk.demo.domain.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;

@MappedSuperclass
public class PersonModel extends BaseEntityModel {
    @Column(name = "name")
    @NotEmpty
    private String firstName;
}
