package com.vladyslav.obochuk.demo.domain.repository;

import com.vladyslav.obochuk.demo.domain.model.IssueModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface IssueRepository extends CrudRepository<IssueModel, Integer> {
    @Query("SELECT issue FROM issues WHERE issue.title LIKE :assigneeName%")
    Collection<IssueModel> findByAssignee(final String assigneeName);
}
