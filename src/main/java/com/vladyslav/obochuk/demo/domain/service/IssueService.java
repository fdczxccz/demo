package com.vladyslav.obochuk.demo.domain.service;

import com.vladyslav.obochuk.demo.domain.model.IssueModel;

import java.util.Collection;
import java.util.Optional;

public interface IssueService {
    Collection<IssueModel> getAllIssues();

    Collection<IssueModel> getIssuesByAssignee(final String name);

    Optional<IssueModel> getIssueById(final int id);
}
