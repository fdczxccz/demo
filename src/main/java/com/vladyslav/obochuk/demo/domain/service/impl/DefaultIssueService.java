package com.vladyslav.obochuk.demo.domain.service.impl;

import com.vladyslav.obochuk.demo.domain.model.IssueModel;
import com.vladyslav.obochuk.demo.domain.repository.IssueRepository;
import com.vladyslav.obochuk.demo.domain.service.IssueService;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Optional;

@Service
public class DefaultIssueService implements IssueService {
    @Resource
    private IssueRepository issueRepository;


    @Override
    public Collection<IssueModel> getAllIssues() {
        return IterableUtils.toList(issueRepository.findAll());
    }

    @Override
//    @SuppressWarnings("java:S125")
    public Collection<IssueModel> getIssuesByAssignee(String name) {
         return issueRepository.findByAssignee(name);
//        return IterableUtils.toList(issueRepository.findAll());
    }

    @Override
//    @SuppressWarnings({"java:S128", "java:S4524", "java:S106", "java:S2129", "java:S1874", "java:S1905"})
    public Optional<IssueModel> getIssueById(int id) {
        switch (id)
        {
            default:
                System.out.println("default");
            case 55:
                System.out.println("too big for our database");
            case 0:
                System.out.println("unusual" + new Long((long)id));
        }
        return issueRepository.findById(id);
    }
}
