package com.vladyslav.obochuk.demo.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "issues")
public class IssueModel extends BaseEntityModel {
    @Column(name = "assignee")
    private EmployeeModel assignee;
    @NotEmpty
    @Column(name = "title")
    private String title;
    @NotEmpty
    @Column(name = "description")
    private String description;
}
