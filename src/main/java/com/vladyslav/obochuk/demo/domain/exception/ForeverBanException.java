package com.vladyslav.obochuk.demo.domain.exception;

public class ForeverBanException extends Error {
    public ForeverBanException(String message) {
        super(message);
    }
}
