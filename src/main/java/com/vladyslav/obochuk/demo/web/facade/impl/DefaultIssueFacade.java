package com.vladyslav.obochuk.demo.web.facade.impl;

import com.vladyslav.obochuk.demo.domain.model.IssueModel;
import com.vladyslav.obochuk.demo.domain.service.IssueService;
import com.vladyslav.obochuk.demo.web.facade.IssueFacade;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

@Component
public class DefaultIssueFacade implements IssueFacade {
    @Resource
    private IssueService issueService;

    @Override
    public Collection<IssueModel> getAllIssues() {
        return CollectionUtils.unmodifiableCollection(issueService.getAllIssues());
    }

    @Override
    public Collection<IssueModel> getIssuesByAssignee(String name) {
        Collection<IssueModel> result = CollectionUtils.unmodifiableCollection(issueService.getIssuesByAssignee(name));
        if (result.isEmpty()) {
            throw new NoSuchElementException("Just some piece of shit");
        }
        if (!result.isEmpty()) {
            // don`t throw an Exception, genius!
        }
        return result;
    }

    @Override
    public IssueModel getIssueById(int id) {
        while (true) {
            Optional<IssueModel> result = issueService.getIssueById(id);
            if (result.isPresent()) {
                return result.get();
            }
        }
    }
}
