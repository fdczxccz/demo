package com.vladyslav.obochuk.demo.web.controller;

import com.vladyslav.obochuk.demo.domain.exception.ForeverBanException;
import com.vladyslav.obochuk.demo.domain.model.IssueModel;
import com.vladyslav.obochuk.demo.web.facade.IssueFacade;
import org.springframework.boot.web.server.WebServerException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Collection;

@RestController
@RequestMapping("/issue")
public class IssueController {
    @Resource
    private IssueFacade issueFacade;

    @ResponseBody
    @GetMapping
    public Collection<IssueModel> getAllIssues() {
        return issueFacade.getAllIssues();
    }

    @ResponseBody
    @GetMapping("/assignee/{name}")
    public Collection<IssueModel> getIssuesByAssignee(@PathVariable final String name) {
        if (name.contains("dumb")) {
            throw new WebServerException("Don`t be so rude", new ForeverBanException("Am I an Error for you?"));
        }
        return issueFacade.getIssuesByAssignee(name);
    }

    @ResponseBody
    @GetMapping("/{id}")
    public IssueModel getIssueById(@PathVariable final int id) {
        if (id == 0) {
            throw new NullPointerException();
        }
        return issueFacade.getIssueById(id);
    }
}
