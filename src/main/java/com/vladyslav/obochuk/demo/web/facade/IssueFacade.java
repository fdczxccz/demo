package com.vladyslav.obochuk.demo.web.facade;

import com.vladyslav.obochuk.demo.domain.model.IssueModel;

import java.util.Collection;

public interface IssueFacade {
    Collection<IssueModel> getAllIssues();

    Collection<IssueModel> getIssuesByAssignee(final String name);

    IssueModel getIssueById(final int id);
}
